"use strict";
(() => {
var exports = {};
exports.id = 586;
exports.ids = [586];
exports.modules = {

/***/ 7611:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ API_URL)
/* harmony export */ });
/* unused harmony export NEXT_URL */
const dev = (/* unused pure expression or super */ null && ("production" !== 'production'));
const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND || "http://localhost:3000/";
const API_URL = process.env.BACKEND || "http://localhost:9999/";


/***/ }),

/***/ 9868:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _config_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7611);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    console.log("here");
    if (req.method === 'PUT') {
        const { email  } = req.body;
        const forgot = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_0__/* .API_URL */ .T}auth/forgot-password`, {
            method: "PUT",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email
            })
        }).then((response)=>{
            return response.json();
        }).then((data)=>{
            if (data.error) {
                console.log(data.error);
            } else {
                return data;
            }
        }).catch((err)=>{
            console.log(err);
        });
        if (forgot.message) {
            res.status(200).json({
                message: forgot.message
            });
        } else {
            res.status(204).json({
                error: "Something wrong happened"
            });
        }
    } else {
        res.setHeader('Allow', [
            'PUT'
        ]);
        res.status(405).json({
            message: `Method ${req.method} not allowed`
        });
    }
});


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(9868));
module.exports = __webpack_exports__;

})();