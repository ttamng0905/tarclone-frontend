"use strict";
(() => {
var exports = {};
exports.id = 994;
exports.ids = [994];
exports.modules = {

/***/ 7611:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ API_URL)
/* harmony export */ });
/* unused harmony export NEXT_URL */
const dev = (/* unused pure expression or super */ null && ("production" !== 'production'));
const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND || "http://localhost:3000/";
const API_URL = process.env.BACKEND || "http://localhost:9999/";


/***/ }),

/***/ 7202:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _config_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7611);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    let contractorId;
    if (req.method === 'POST') {
        // const parsedObject = JSON.parse(req.body);
        // console.log("here")
        // console.log(parsedObject);
        // console.log(JSON.parse(req.body));
        const { email , password  } = req.body;
        const contractorLogin = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_0__/* .API_URL */ .T}auth/contractorlogin`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email,
                password
            })
        }).then((response)=>{
            return response.json();
        }).then((response)=>{
            if (response['token']) {
                // localStorage.setItem('jwtToken', JSON.stringify(response['token']));
                // localStorage.setItem('contractorId', response['contractorId']);
                contractorId = response['contractorId'];
            // } else {
            //     this.setState({ error: response })
            }
            // console.log(response);
            return response;
        }).catch((err)=>console.log(err)
        );
        console.log(contractorLogin);
        const userData = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_0__/* .API_URL */ .T}api/contractor/${contractorId}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        }).then((response)=>{
            const data = response.json();
            return data;
        }).catch((err)=>console.log(err)
        );
        if (userData["contractor"]) {
            //@todo: Set Cookies
            res.status(200).json({
                user: userData["contractor"]
            });
        } else {
            res.status(204).json({
                error: "User does not exist"
            });
        }
    } else {
        res.setHeader('Allow', [
            'POST'
        ]);
        res.status(405).json({
            message: `Method ${req.method} not allowed`
        });
    }
});


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(7202));
module.exports = __webpack_exports__;

})();