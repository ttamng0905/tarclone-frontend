"use strict";
(() => {
var exports = {};
exports.id = 541;
exports.ids = [541];
exports.modules = {

/***/ 7611:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ API_URL)
/* harmony export */ });
/* unused harmony export NEXT_URL */
const dev = (/* unused pure expression or super */ null && ("production" !== 'production'));
const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND || "http://localhost:3000/";
const API_URL = process.env.BACKEND || "http://localhost:9999/";


/***/ }),

/***/ 8513:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ user)
});

;// CONCATENATED MODULE: external "cookie"
const external_cookie_namespaceObject = require("cookie");
var external_cookie_default = /*#__PURE__*/__webpack_require__.n(external_cookie_namespaceObject);
// EXTERNAL MODULE: ./config/index.js
var config = __webpack_require__(7611);
;// CONCATENATED MODULE: ./pages/api/user.js


/* harmony default export */ const user = (async (req, res)=>{
    if (req.method === 'POST') {
        // console.log("here in user");
        // if (!req.headers.cookie) {
        //     res.status(403).json({Message: 'Not Authorized'})
        //     return
        // }
        const cookies = req.headers.cookie;
        if (typeof cookies === 'string') {
            const cookieJson = external_cookie_default().parse(cookies);
            console.log("I am here in user.js " + cookieJson.token);
            console.log(cookieJson.token);
        }
        const { token  } = req.body;
        console.log(token);
        const currentUser = await fetch(`${config/* API_URL */.T}auth/current`, {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                token
            })
        });
        const user = await currentUser.json();
        console.log(user);
        if (currentUser) {
            res.status(200).json({
                user
            });
        } else {
            res.status(403).json({
                message: 'User forbidden'
            });
        }
    } else {
        res.setHeader('Allow', [
            'POST'
        ]);
        res.status(405).json({
            message: `Method ${req.method} not allowed`
        });
    }
});


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(8513));
module.exports = __webpack_exports__;

})();