"use strict";
(() => {
var exports = {};
exports.id = 553;
exports.ids = [553];
exports.modules = {

/***/ 7611:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "T": () => (/* binding */ API_URL)
/* harmony export */ });
/* unused harmony export NEXT_URL */
const dev = (/* unused pure expression or super */ null && ("production" !== 'production'));
const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND || "http://localhost:3000/";
const API_URL = process.env.BACKEND || "http://localhost:9999/";


/***/ }),

/***/ 8307:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _config_index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7611);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    if (req.method === 'POST') {
        const { company_name , email , password , password2 , address , city , state , zip , phone , mobile , employees_number , founded , doEmergency  } = req.body;
        console.log(req.body);
        console.log(typeof req.body);
        const contractorRegistration = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_0__/* .API_URL */ .T}auth/contractorsignup`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                company_name,
                email,
                password,
                password2,
                address,
                city,
                state,
                zip,
                phone,
                mobile,
                employees_number,
                founded,
                doEmergency
            })
        }).then((response)=>{
            console.log("here");
            // console.log(response)
            return response.json();
        }).catch((err)=>console.log(err)
        );
        console.log(contractorRegistration);
        if (contractorRegistration) {
            //@todo: Set Cookies
            res.status(200).json({
                user: contractorRegistration
            });
        } else {
            res.status(204).json({
                error: "Something went wrong"
            });
        }
    } else {
        res.setHeader('Allow', [
            'POST'
        ]);
        res.status(405).json({
            message: `Method ${req.method} not allowed`
        });
    }
});


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(8307));
module.exports = __webpack_exports__;

})();