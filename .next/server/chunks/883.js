"use strict";
exports.id = 883;
exports.ids = [883];
exports.modules = {

/***/ 1454:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Resources)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(968);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1664);
/* harmony import */ var _system__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3125);





const buttons = [
    {
        text: "System Type & Info",
        urlId: "system"
    },
    {
        text: "Sustainability",
        urlId: "sustainability"
    },
    {
        text: "Energy Savings",
        urlId: "energysavings"
    }, 
];
function Resources() {
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "bg-white",
        children: [
            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((next_head__WEBPACK_IMPORTED_MODULE_2___default()), {
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("title", {
                        children: "Resources | Tarclone"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "robots",
                        content: "all"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("meta", {
                        name: "keywords",
                        content: "hvac resources, hvac prices, sustainability, energy savings, homeowners, contractors, hvac, air conditioning, heat pumps, air filters, maintenance"
                    })
                ]
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "max-w-7xl mx-auto pt-8 md:pt-16 pb-6 px-4 sm:py-24 sm:pb-8 sm:px-6 lg:px-8",
                children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: "text-center",
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
                            className: "text-base font-semibold text-blue-500 tracking-wide uppercase",
                            children: "Resources"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            className: "mt-2 md:text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl",
                            children: "Helping You Make Informed Decisions"
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            className: "max-w-xl mt-2 mx-auto md:text-xl text-gray-500",
                            children: "Understand different HVAC types and see what's ahead for a sustainable future in the industry."
                        })
                    ]
                })
            }),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "mx-auto pb-3",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: "text-center",
                    children: buttons.map((button)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_3__["default"], {
                                href: `/resources/${button.urlId}`,
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                    className: "inline-flex items-center px-4 py-2 mx-5 mb-3 border border-gray-300 shadow-sm text-sm font-medium rounded-md hover:bg-gray-100",
                                    children: button.text
                                })
                            })
                        })
                    )
                })
            })
        ]
    }));
};


/***/ }),

/***/ 3125:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ System),
/* harmony export */   "getStaticProps": () => (/* binding */ getStaticProps)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5675);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1664);
/* harmony import */ var _index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1454);




function System(props) {
    const { systems  } = props;
    return(/*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "bg-white",
        children: [
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_index__WEBPACK_IMPORTED_MODULE_3__["default"], {}),
            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "mx-7 sm:mx-0 md:container md:mx-auto lg:px-40",
                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("ul", {
                    className: "divide-y divide-gray-200",
                    children: systems.map((item)=>/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                            className: "py-4",
                            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: "lg:mx-5 md:mx-3",
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h5", {
                                        className: "text-base font-semibold text-blue-500 tracking-wide",
                                        children: item.system_type
                                    }),
                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                        className: "grid lg:grid-cols-6 sm:grid-cols-1",
                                        children: [
                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                className: "lg:col-span-1 sm:col-span-5 object-contain my-2 md:my-6",
                                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_image__WEBPACK_IMPORTED_MODULE_1__["default"], {
                                                    src: item.image,
                                                    width: 336,
                                                    height: 336
                                                })
                                            }),
                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                                className: "sm:pl-5 lg:col-span-5",
                                                children: [
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                        className: "lg:mt-5 sm:my-1",
                                                        children: [
                                                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
                                                                className: "font-bold",
                                                                children: [
                                                                    "Cost Including Install:",
                                                                    " "
                                                                ]
                                                            }),
                                                            item.cost
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                        className: "my-1",
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "font-bold",
                                                                children: "About: "
                                                            }),
                                                            item.about
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                                                                className: "font-bold",
                                                                children: "When to Use: "
                                                            }),
                                                            item.when_to_use
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("ul", {
                                                        children: [
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                                children: item.when_to_use_one
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                                children: item.when_to_use_two
                                                            }),
                                                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("li", {
                                                                children: item.when_to_use_three
                                                            })
                                                        ]
                                                    }),
                                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__["default"], {
                                                            href: item.link,
                                                            type: "button",
                                                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                                                                className: "inline-flex px-4 py-2 mt-2 mb-3 border border-gray-300 shadow-sm text-sm font-medium rounded-md hover:bg-gray-100",
                                                                children: "Learn more"
                                                            })
                                                        })
                                                    })
                                                ]
                                            })
                                        ]
                                    })
                                ]
                            })
                        }, item.system_type)
                    )
                })
            })
        ]
    }));
};
async function getStaticProps() {
    return {
        props: {
            systems: [
                {
                    cost: "$6,000 - $10,000",
                    system_type: "Indoor Furnace and A/C System",
                    about: "Most popular system, and often known as Central A/C",
                    when_to_use_one: "- Indoor space has ductwork",
                    when_to_use_two: "- New build",
                    when_to_use_three: "- Replacing an existing system",
                    image: "/images/resources/system/furnace_ac.jpeg",
                    link: "/resources/system/indoorfurnaceac"
                },
                {
                    cost: "$2,000 - $5,000",
                    system_type: "Indoor Furnace Only",
                    about: "Produces heat only and can be paired with A/C",
                    when_to_use_one: "- Indoor space has ductwork",
                    when_to_use_two: "- Replace existing furnace",
                    image: "/images/resources/system/furnace.jpeg",
                    link: "/resources/system/indoorfurnaceac"
                },
                {
                    cost: "$4,000 - $7,000",
                    system_type: "A/C System (Outdoor Unit & Indoor Coil)",
                    about: "Produces cool air and can be added to a furnace",
                    when_to_use_one: "- Indoor space has ductwork",
                    when_to_use_two: "- Enough room near furnace to add coil",
                    image: "/images/resources/system/ac_coil2.jpeg",
                    link: "/resources/system/indoorfurnaceac"
                },
                {
                    cost: "$5,000 - $15,000",
                    system_type: "Ductless Mini-Split System",
                    about: "Extremely efficient, all electric, and no ductwork needed",
                    when_to_use_one: "- Indoor space has radiant heating with no ductwork",
                    when_to_use_two: "- You're okay with an indoor unit in each room",
                    when_to_use_three: "- Prefer to use electric heat instead of gas",
                    image: "/images/resources/system/ductless.jpeg",
                    link: "/resources/system/ductlesssplit"
                },
                {
                    cost: "$6,000 - $10,000",
                    system_type: "Heat Pump System",
                    about: "All electric and ready for the sustainable future",
                    when_to_use_one: "- Indoor space has ductwork",
                    when_to_use_two: "- Replace existing heatpump or A/C system",
                    when_to_use_three: "- Prefer to use electric heat instead of gas heat",
                    image: "/images/resources/system/heatpump.jpeg",
                    link: "/resources/system/heatpump"
                },
                {
                    cost: "$1,000 - $2,000",
                    system_type: "Swamp Cooler",
                    about: "Great for dry climates and very affordable",
                    when_to_use_one: "- Indoor space has ductwork",
                    when_to_use_two: "- Looking for an affordable and efficient cooling solution",
                    when_to_use_three: "- You're okay with adding humidity to the space",
                    image: "/images/resources/system/swampcooler.jpeg",
                    link: "/resources/system/swampcooler"
                }, 
            ]
        }
    };
}


/***/ })

};
;