"use strict";
exports.id = 531;
exports.ids = [531];
exports.modules = {

/***/ 2500:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "G": () => (/* binding */ NEXT_URL),
/* harmony export */   "T": () => (/* binding */ API_URL)
/* harmony export */ });
const dev = (/* unused pure expression or super */ null && ("production" !== 'production'));
const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND || "http://localhost:3000/";
const API_URL = process.env.BACKEND || "http://localhost:9999/";


/***/ }),

/***/ 9531:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "H": () => (/* binding */ AuthProvider),
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _config_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2500);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9915);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([js_cookie__WEBPACK_IMPORTED_MODULE_4__]);
js_cookie__WEBPACK_IMPORTED_MODULE_4__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];





const AuthContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)();
const AuthProvider = ({ children  })=>{
    const { 0: user1 , 1: setUser  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    const { 0: error , 1: setError  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    const { 0: token , 1: setToken  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(null);
    const router = (0,next_router__WEBPACK_IMPORTED_MODULE_3__.useRouter)();
    const contractor = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)();
    //Check if user is logged in
    const checkUserLoggedIn = async ()=>{
        contractor.current = JSON.parse(localStorage.getItem("user"));
        console.log(contractor);
        if (contractor.current) {
            setUser(contractor.current);
        }
    // } else {
    //     const res = await fetch(`${NEXT_URL}api/user`, {
    //         method: 'POST',
    //         headers: {
    //             Accept: "application/json",
    //             "Content-Type": "application/json"
    //         },
    //         body: JSON.stringify({
    //             token
    //         })
    //     })
    //     console.log(res);
    //     if(res){
    //         setUser(res.contractor);
    //         // await router.push('/')
    //     } else {
    //         setUser(null)
    //     }
    // }
    };
    (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(()=>checkUserLoggedIn
    , []);
    //Register User
    const register = async ({ company_name , email , password , password2 , address , city , state , zip , phone , mobile , employees_number , founded , doEmergency  })=>{
        const contractorRegistration = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_2__/* .API_URL */ .T}auth/contractorsignup`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                company_name,
                email,
                password,
                password2,
                address,
                city,
                state,
                zip,
                phone,
                mobile,
                employees_number,
                founded,
                doEmergency
            })
        }).then((response)=>{
            // console.log(response)
            return response.json();
        }).catch((err)=>console.log(err)
        );
    // if(contractorRegistration){
    //     // res.status(200).json({user: contractorRegistration})
    // } else {
    //     // res.status(204).json({error: "Something went wrong"})
    // }
    };
    //Forgot Password
    // const forgot = async() => {
    //     const data = await fetch(`${API_URL}/auth/forgot-password`, {
    //         method: "PUT",
    //         headers: {
    //             Accept: "application/json",
    //             "Content-Type": "application/json",
    //         },
    //         body: JSON.stringify({
    //             email
    //         })
    //     })
    //         .then((response) => {
    //             console.log("forgot password response: ", response)
    //             return response.json();
    //
    //         })
    //         .then(data => {
    //             if (data.error) {
    //                 console.log(data.error)
    //             }
    //             else {
    //                 return data;
    //             }
    //         }).catch(err => {
    //             console.log(err)
    //         });
    //     if(data.message){
    //         res.status(200).json({message: forgot.message})
    //     } else {
    //         res.status(204).json({error: "Something wrong happened"})
    //     }
    // }
    //Login User
    const login = async (user)=>{
        let contractorId;
        const email = user.email;
        const password = user.password;
        const contractorLogin = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_2__/* .API_URL */ .T}auth/contractorlogin`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email,
                password
            })
        }).then((response)=>{
            return response.json();
        }).then((response)=>{
            if (response['token']) {
                contractorId = response['contractorId'];
            }
            return response;
        }).catch((err)=>console.log(err)
        );
        const userData = await fetch(`${_config_index__WEBPACK_IMPORTED_MODULE_2__/* .API_URL */ .T}api/contractor/${contractorId}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        }).then((response)=>{
            const data = response.json();
            return data;
        }).catch((err)=>console.log(err)
        );
        console.table(userData["contractor"]);
        if (userData["contractor"]) {
            await setUser(userData["contractor"]);
            await localStorage.setItem("user", JSON.stringify(userData["contractor"]));
        } else {
            setError(null);
        }
        if (userData["contractor"]) {
            // @todo: Set Cookies
            setToken(contractorLogin.token);
            js_cookie__WEBPACK_IMPORTED_MODULE_4__["default"].set('token', contractorLogin.token);
        }
    };
    //Logout User
    const logout = async ()=>{
        setUser(null);
        console.log("me");
        localStorage.removeItem("user");
    };
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(AuthContext.Provider, {
        value: {
            user: user1,
            error,
            register,
            login,
            logout
        },
        children: children
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AuthContext);

__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;