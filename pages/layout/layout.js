import {Fragment, useContext, useEffect, useRef} from "react";
import Navigation from "./navigation";
import Footer from "./footer";
import AuthContext from "@/context/AuthContext";

function Layout(props){
    const con = useRef(null);
    const { user } = useContext(AuthContext);
    useEffect(() => {
        if (user) {
            con.current = user;
        }
        if (localStorage.getItem("user") != null) {
            con.current = JSON.parse(localStorage.getItem("user"));
        }

    }, [user]);
    return <Fragment>
        {con.current ? "" : <Navigation/>}
        <main>
            {props.children}
        </main>
        <Footer/>
    </Fragment>
}

export default Layout;
