import "../styles/globals.css";
import Head from "next/head";
import Layout from "./layout/layout";
import { AuthProvider } from "@/context/AuthContext";

function MyApp({ Component, pageProps }) {
	return (
		<AuthProvider>
			<Layout>
				<Head>
					<link rel="shortcut icon" href="/favicon_64px.png" />
				</Head>
				<Component {...pageProps} />
			</Layout>
		</AuthProvider>
	);
}

export default MyApp;
