import {API_URL} from '@/config/index';


export default async (req, res) => {
    let contractorId;
    if(req.method === 'POST') {
        // const parsedObject = JSON.parse(req.body);
        // console.log("here")
        // console.log(parsedObject);
        // console.log(JSON.parse(req.body));
        const {email, password} = req.body;

        const contractorLogin = await fetch(`${API_URL}auth/contractorlogin`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email,
                password
            })
        })
            .then(response => {
                return response.json();
            })
            .then(response => {
                if (response['token']) {
                    // localStorage.setItem('jwtToken', JSON.stringify(response['token']));
                    // localStorage.setItem('contractorId', response['contractorId']);
                    contractorId = response['contractorId'];
                // } else {
                //     this.setState({ error: response })
                }
                // console.log(response);
                return response
            })
            .catch(err => console.log(err))

        console.log(contractorLogin);
        const userData = await fetch(`${API_URL}api/contractor/${contractorId}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                const data = response.json();
                return data;
            })
            .catch(err => console.log(err))



        if(userData["contractor"]){
            //@todo: Set Cookies
            res.status(200).json({user: userData["contractor"]})
        } else {
            res.status(204).json({error: "User does not exist"})
        }
    } else {
        res.setHeader('Allow', ['POST'])
        res.status(405).json({message: `Method ${req.method} not allowed`});
    }
}
