import jsHttpCookie from 'cookie';
import {API_URL} from '@/config/index';


export default async (req, res) => {
    if(req.method === 'POST') {
        // console.log("here in user");
        // if (!req.headers.cookie) {
        //     res.status(403).json({Message: 'Not Authorized'})
        //     return
        // }
        const cookies = req.headers.cookie;
        if (typeof cookies === 'string'){
            const cookieJson = jsHttpCookie.parse(cookies);
            console.log("I am here in user.js " + cookieJson.token);
            console.log(cookieJson.token);
        }
        const {token} = req.body;
        console.log(token);
        const currentUser = await fetch(`${API_URL}auth/current`, {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({token})
        })
        const user = await currentUser.json()
        console.log(user)
        if (currentUser){
            res.status(200).json({user})
        } else {
            res.status(403).json({message: 'User forbidden'})
        }

    } else {
        res.setHeader('Allow', ['POST'])
        res.status(405).json({message: `Method ${req.method} not allowed`});
    }
}
