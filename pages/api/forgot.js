import {API_URL} from '@/config/index';


export default async (req, res) => {

    console.log("here");
    if(req.method === 'PUT') {
        const {email} = req.body;

        const forgot = await fetch(`${API_URL}auth/forgot-password`, {
            method: "PUT",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email
            })
        })
            .then((response) => {
                return response.json();

            })
            .then(data => {
                if (data.error) {
                    console.log(data.error)
                }
                else {
                    return data;
                }
            }).catch(err => {
                console.log(err)
            });
        if(forgot.message){
            res.status(200).json({message: forgot.message})
        } else {
            res.status(204).json({error: "Something wrong happened"})
        }
    } else {
        res.setHeader('Allow', ['PUT'])
        res.status(405).json({message: `Method ${req.method} not allowed`});
    }
}
