import React, { useState, useContext } from 'react';
import { useRouter } from "next/router";
import {NEXT_URL} from "@/config/index";

export default function ForgotPassword() {
    const router = useRouter();
    const [email, setEmail] = useState("")


    const forgotPassword = async () => {
        console.log("heaaaa");
        await fetch(`${NEXT_URL}/api/forgot`, {
            method: "PUT",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email
            })
        })
            .then((response) => {
                console.log("forgot password response: ", response)
                return response.json();

            })
            .then(data => {
                if (data.error) {
                    console.log(data.error)
                }
                else {
                    console.log(data.message);

                }
            }).catch(err => {
            console.log(err)
        });

        console.log("heaaaa 1111");
        await router.push('/login')
    }
    return (

        <div className="mt-10 pt-16 pb-40 px-8">
            <div className="mx-auto w-full max-w-sm lg:w-96">
                <h2 className="mt-6 text-3xl text-center font-extrabold text-gray-900">Enter your email</h2>

                <form className="space-y-6">
                    <div className="form-group mt-8">
                        <input
                            type="email"
                            className="appearance-none block w-full px-3 py-2 border border-gray-400 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm"
                            placeholder="Your email address"
                            value={email}
                            name="email"
                            onChange={(e) => setEmail(e.target.value)}
                            autoFocus
                        />
                    </div>
                    {/* Refactor messages below */}
                    {/* <div className="my-4">
                            {this.state.message && (
                                <h4 className="text-lg font-semibold">{this.state.message}</h4>
                            )}
                            {this.state.error && (
                                <h4 className="text-lg font-semibold">{this.state.error}</h4>
                            )}
                        </div> */}
                    <button
                        type="submit"
                        onClick={() => forgotPassword()}
                        className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                    >
                        Submit
                    </button>
                </form>
                <div className="mt-4"><p className="text-sm">Reset Link will be sent to the entered email when you hit submit</p></div>
            </div>
        </div>

    )
}
