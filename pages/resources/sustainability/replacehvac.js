import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";

export default function ReplaceHvac() {
	return (
		<div className="replacehvac">
			<Head>
				<title>HVAC Replacement | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="new hvac system, hvac service, boulder, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>
			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								Sustainability
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								Replacing HVAC Equipment
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/sustainability/replace_hvac.jpg"
								alt="Air Conditioners on side of House"
								width={1024}
								height={512}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Replacing HVAC Equipment
						</h3>
						<p>
							If your HVAC equipment is over 10 years old, it may be
							advantageous to think about replacing equipment when something
							goes wrong instead of simply repairing it. This may help you save
							money long-term by not repairing the unit every few years or using
							a higher efficient model. From a sustainability standpoint, the
							new unit may have a more eco-friendly refrigerant or you have the
							ability to move to an all-electric system.
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Costly Repairs
						</h3>
						<p>
							As your equipment gets older, it makes sense that you’ll need to
							service the equipment more often. If the HVAC system has been
							properly maintained throughout its life, the system typically
							lasts longer and needs less repair look. It’s up to you to
							determine the economic trade-off between how often you’d need to
							repair the unit at an older age or replace the equipment with new
							equipment requiring no repair work.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							High Efficiency Models
						</h3>
						<p>
							If you have an older HVAC system, it’s almost certain newer HVAC
							equipment will be much more efficient. Furthermore, local utility
							companies often time have incentives and rebates in line with the
							efficiency of new equipment. Browse your utility company’s website
							to find out what rebates are offered.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Sustainability
						</h3>
						<p>
							Policies and laws are continually changing to make the world more
							sustainable, and for HVAC, refrigerants are being regulated to
							keep our ozone health and global warming in check. When moving
							from an older HVAC system to a newer one, it’s likely that the new
							refrigerant will be more eco-friendly.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							All-Electric Equipment
						</h3>
						<p>
							If you’re looking to replace a traditional, central air, type
							system it likely uses gas heat. There’s an opportunity for you to
							purchase a heat pump system as a replacement. Heat pumps utilize
							electric cooling and heating. In colder climates, you may need to
							tack on some additional electric heat to meet the load.
						</p>
						{/* <CallToAction /> */}
						<NewsletterSignUp />
					</div>
				</div>
			</div>
		</div>
	);
}
