import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";

export default function Refrigerants() {
	return (
		<div className="refrigerants">
			<Head>
				<title>Refrigerants | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="refrigerant, hvac service, boulder, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>
			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								Sustainability
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								HVAC Refrigerants
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/sustainability/refrigerant_hvac.jpg"
								alt="A/C being serviced"
								width={1024}
								height={512}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Refrigerants
						</h3>
						<p>
							Refrigerants are used in HVAC systems, cars, and refrigeration
							equipment throughout the world. Its abundance is both a blessing
							and burden considering it’s a need in today’s world, but
							refrigerants aren’t good for the environment when released into
							the air.
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Refrigerant Characteristics
						</h3>
						<p>
							Refrigerants are typically characterized by their efficiency and
							cooling capacity. For sustainable purposes, they’re also
							characterized by their Ozone Depletion Potential (ODP) and Global
							Warming Potential (GWP). Regulations have been put in place to
							limit refrigerants’ detrimental impact to the environment. In your
							residential HVAC system, you may be using R-22 or R-410a as your
							refrigerant. R-22 is worse for the environment than R-410a, and
							eventually a new refrigerant will replace R-410a as a greener
							option. It’s worth checking or asking a contractor for the type of
							refrigerant that will be used in your system.
						</p>
						<br />

						<table className="text-xs md:text-sm border-collapse table-auto border border-slate-400">
							<thead>
								<tr>
									<th class="border border-slate-300 p-1 text-center">
										Environment Effect
									</th>
									<th class="border border-slate-300 p-1 text-center ">
										Chloro- fluoro- carbon (CFC)
									</th>
									<th class="border border-slate-300 p-1 text-center ">
										Hydro- chloro- fluoro- carbon (HCFC)
									</th>
									<th class="border border-slate-300 p-1 text-center ">
										Hydro- fluoro- carbon (HFC)
									</th>
									<th class="border border-slate-300 p-1 text-center ">
										Hydro- fluoro- olefin (HFO)
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="border border-slate-300 p-1 text-center ">ODP</td>
									<td class="border border-slate-300 p-1 text-center ">
										Very High
									</td>
									<td class="border border-slate-300 p-1 text-center ">
										Very Low
									</td>
									<td class="border border-slate-300 p-1 text-center ">None</td>
									<td class="border border-slate-300 p-1 text-center ">None</td>
								</tr>
								<tr>
									<td class="border border-slate-300 p-1 text-center ">GWP</td>
									<td class="border border-slate-300 p-1 text-center ">
										Very High
									</td>
									<td class="border border-slate-300 p-1 text-center ">High</td>
									<td class="border border-slate-300 p-1 text-center ">
										Low to High
									</td>
									<td class="border border-slate-300 p-1 text-center ">
										Very Low
									</td>
								</tr>
								<tr>
									<td class="border border-slate-300 p-1 text-center">
										Example
									</td>
									<td class="border border-slate-300 p-1 text-center ">R11</td>
									<td class="border border-slate-300 p-1 text-center ">R22</td>
									<td class="border border-slate-300 p-1 text-center ">
										R410a, R32
									</td>
									<td class="border border-slate-300 p-1 text-center ">TBD</td>
								</tr>
							</tbody>
						</table>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							HVAC System Leaks
						</h3>
						<p>
							One way for refrigerants to enter the atmosphere is a leak in your
							HVAC system. This is common and is often found out when something
							goes wrong with your system. A better approach would be to use a
							contractor for annual (or bi-annual) preventive maintenance checks
							where the leak can be detected early on and fixed. This also
							increases the life and efficiency of your equipment.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							HVAC Replacement Procedures
						</h3>
						<p>
							Another way refrigerants sneak into the environment is through
							poor transportation and storage. When HVAC is replaced, please
							check with your contractor to make sure the refrigerant from the
							old system is recaptured and recycled. Refrigerants released in a
							landfill or improperly disposed is detrimental to the ozone.
						</p>
						{/* <CallToAction /> */}
						<NewsletterSignUp />
					</div>
				</div>
			</div>
		</div>
	);
}
