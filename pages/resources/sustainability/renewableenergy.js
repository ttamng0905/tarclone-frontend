import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";

export default function RenewableEnergy() {
	return (
		<div className="renewableenergy">
			<Head>
				<title>Renewables | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="renewable energy, hvac service, boulder, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>
			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								Sustainability
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								Solar - Renewable Energy
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/sustainability/solar_home.jpg"
								alt="Home with solar panels"
								width={1024}
								height={512}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Renewable Energy
						</h3>
						<p>
							If sustainable and renewable energy is at the top of your list
							when making your next HVAC purchase, there are options that may be
							more appealing to you. A traditional system includes electric
							cooling and natural gas heating. There are options for an
							all-electric solution.
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Heat Pump Systems
						</h3>
						<p>
							A heat pump system utilizes electric cooling an electric heating.
							In colder climates, you may need to purchase an additional
							electric heating coil to meet your space’s cooling load during the
							winter. There are heat pump options when replacing a traditional
							ducted system and also a system without ductwork.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Solar Power
						</h3>
						<p>
							If you plan to purchase solar for your home soon, you may want to
							plan for a new, all-electric heat pump system. You’ll want to
							factor this in when deciding how many solar panels are needed. A
							heat pump system will reduce your natural gas use but increase
							your electricity use.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Utility Renewable Energy Programs
						</h3>
						<p>
							Depending on your region, local utility providers may have
							renewable energy programs. Utility providers that have purchased
							power from renewable energy sources or have installed their own
							renewable energy source, will offer consumers the ability to
							opt-in to the program. The idea is that a consumer opting into the
							program is purchasing the power from the renewable energy
							resources rather than from coal or gas plants. In this case,
							having an all-electric HVAC system may make sense to have the feel
							of using only renewable energy.
						</p>

						<NewsletterSignUp />
						{/* <CallToAction /> */}
					</div>
				</div>
			</div>
		</div>
	);
}
