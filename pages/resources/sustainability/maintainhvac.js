import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";
import { BanIcon } from "@heroicons/react/outline";

const merv8items = [
	{
		title: "Lint",
		icon: BanIcon,
	},
	{
		title: "Dust",
		icon: BanIcon,
	},
	{
		title: "Pollen",
		icon: BanIcon,
	},
	{
		title: "Mites",
		icon: BanIcon,
	},
	{
		title: "Mold",
		icon: BanIcon,
	},
];

const merv13items = [
	{
		title: "Lint",
		icon: BanIcon,
	},
	{
		title: "Dust",
		icon: BanIcon,
	},
	{
		title: "Pollen",
		icon: BanIcon,
	},
	{
		title: "Mites",
		icon: BanIcon,
	},
	{
		title: "Mold",
		icon: BanIcon,
	},
	{
		title: "Pet Dander",
		icon: BanIcon,
	},
	{
		title: "Smoke",
		icon: BanIcon,
	},
	{
		title: "Smog",
		icon: BanIcon,
	},
	{
		title: "Bacteria",
		icon: BanIcon,
	},
	{
		title: "Viruses",
		icon: BanIcon,
	},
];

export default function MaintainHvac() {
	return (
		<div className="maintainhvac">
			<Head>
				<title>HVAC Maintenance | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="hvac maintenance, hvac service, boulder, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>
			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								Sustainability
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								Maintaining Your HVAC System
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/sustainability/maintain_hvac.jpg"
								alt="Person replacing air filter"
								width={1024}
								height={512}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Maintaining HVAC Equipment
						</h3>
						<p>
							With a busy world and the many distractions in life, HVAC isn’t
							typically something that one thinks about often, let alone
							maintaining it. For almost everyone, HVAC maintenance is simply
							changing out an air filter every three months which can also be
							neglected. This begs the question, what does HVAC maintenance look
							like and what are the benefits?
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Replacing Air Filters
						</h3>
						<p>
							It’s recommended to replace disposable air filters every three
							months – more often if you have pets or your air filters fill up
							with lint and dirt at a fast rate. If your filter isn’t replaced
							regularly, you could have issues with your HVAC system. Issues
							such as low airflow, coil freezing, fan failure, etc. For homes, a
							1” pleated filter works perfectly fine at an efficiency rating of
							your choice. MERV Rating Info below. Regular replacement also
							helps save you money given that a full filter makes the supply fan
							work harder, and in turn, use more energy.
						</p>
						{/* add the merv chart */}

						<div className="mt-12 space-y-4 sm:mt-16 sm:space-y-0 sm:grid sm:grid-cols-2 sm:gap-6 lg:max-w-4xl lg:mx-auto xl:max-w-none xl:mx-0 ">
							<div className="border border-gray-800 rounded-xl shadow-sm divide-y divide-gray-200">
								<div className="p-6">
									<h2 className="text-xl text-center font-medium text-gray-900">
										MERV 8
									</h2>
									<p className="text-sm text-center text-gray-500">MPR 600</p>
									<p className="text-sm text-center text-gray-500">FPR 5</p>
								</div>
								<div className="pt-6 pb-8 px-6">
									<h2 className="mb-2">Captures:</h2>
									<ul role="list" className="grid grid-cols-2">
										{merv8items.map((merv8item) => (
											<li
												key={merv8item.title}
												className="flex mt-2 items-center space-x-2"
											>
												<merv8item.icon
													className="flex-shrink-0 h-5 w-5 text-green-500"
													aria-hidden="true"
												/>
												<span className="text-sm text-gray-500">
													{merv8item.title}
												</span>
											</li>
										))}
									</ul>
								</div>
							</div>

							<div className="border border-gray-800 rounded-xl shadow-sm divide-y divide-gray-200">
								<div className="p-6">
									<h2 className="text-xl text-center font-medium text-gray-900">
										MERV 13
									</h2>
									<p className="text-sm text-center text-gray-500">MPR 1500</p>
									<p className="text-sm text-center text-gray-500">FPR 10</p>
								</div>
								<div className="pt-6 pb-8 px-6">
									<h2 className="mb-2">Captures:</h2>
									<ul role="list" className="grid grid-cols-2">
										{merv13items.map((merv13item) => (
											<li
												key={merv13item.title}
												className="flex mt-2 items-center space-x-2"
											>
												<merv13item.icon
													className="flex-shrink-0 h-5 w-5 text-green-500"
													aria-hidden="true"
												/>
												<span className="text-sm text-gray-500">
													{merv13item.title}
												</span>
											</li>
										))}
									</ul>
								</div>
							</div>
						</div>

						{/* <div>{items.map((item) => (
								<div key={item.title} className="relative">
									<dt>
										<div className="text-black bg-blue-100">
											<item.icon className="h-6 w-6" aria-hidden="true" />
										</div>
										<p className="ml-16 text-lg leading-6 font-medium text-gray-900">
											{item.title}
										</p>
									</dt>
									
								</div>
							))}</div> */}

						<br />

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Bi-Annual Preventive Maintenance Check
						</h3>
						<p>
							To ensure that your HVAC system is in tip-top shape, it’s
							important to have your system checked before each heating and
							cooling season. Almost all HVAC contractors will perform bi-annual
							check-ups if an appointment is scheduled, and many are able to
							provide maintenance contracts. During the bi-annual checks, unit
							diagnostics are analyzed, refrigerant levels are checked, and
							heating and cooling elements are tested for the upcoming season.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Clean Ductwork
						</h3>
						<p>
							Not always the highest priority, even for HVAC, clean ductwork can
							be a breath of fresh air (literally). Clean ductwork improves
							indoor air quality and proper airflow. There are specialty
							companies that provide a ductwork cleaning services inexpensively
							and some residential HVAC contractors will do so as well.
						</p>
						{/* <CallToAction /> */}
						<NewsletterSignUp />
					</div>
				</div>
			</div>
		</div>
	);
}
