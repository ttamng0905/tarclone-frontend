import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";
import {
	PlusCircleIcon,
	MinusCircleIcon,
	ExclamationIcon,
} from "@heroicons/react/outline";

export default function HighEfficientHvac() {
	return (
		<div className="highefficiency">
			<Head>
				<title>High Efficiency HVAC | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="efficiency, energy savings, heat pumps, heat pump, boulder, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>
			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								Energy Savings
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								High Efficiency HVAC
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/system/heatpump_resources.jpg"
								alt="Air Conditioners on side of House"
								width={1024}
								height={512}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Energy Efficient HVAC
						</h3>
						<p>
							With advances in the HVAC industry over the past decade, there are
							many new technologies the give homeowner a range of options when
							it comes to HVAC system efficiencies. A higher efficient HVAC
							system leads to a decreased utility bill.
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Replacing HVAC Equipment
						</h3>
						<p>
							If you have an older HVAC system, it’s almost certain newer HVAC
							equipment will be much more efficient. Furthermore, there will be
							less repairs. New HVAC equipment, even at the basic level of
							efficiency, is likely anywhere from 10% to 50% more efficient than
							your old system. This is due to degradation of the unit and better
							government standards.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Utility Provider Incentives
						</h3>
						<p>
							Local utility companies often time have incentives and rebates in
							line with the efficiency of new equipment. Browse your utility
							company’s website to find out what rebates are offered. It’s
							possible it’s a fixed amount incentive if your equipment is over a
							certain efficiency, or it could be tiered to cater to
							progressively higher levels of efficiency.
						</p>

						<NewsletterSignUp />
						{/* <CallToAction /> */}
					</div>
				</div>
			</div>
		</div>
	);
}
