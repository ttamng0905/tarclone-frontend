import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";
import {
	PlusCircleIcon,
	MinusCircleIcon,
	ExclamationIcon,
} from "@heroicons/react/outline";

export default function WiFiThermostat() {
	return (
		<div className="wifithermostat">
			<Head>
				<title>Thermostats | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="thermostats, nest, ecobee, honeywell, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>
			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								Energy Savings
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								Web-Enabled Thermostats
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/savings/nest_thermostat.jpg"
								alt="Thermostat on wall"
								width={1024}
								height={683}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Web-Enabled Thermostats
						</h3>
						<p>
							There are dozens of web-enabled (or Wi-Fi) thermostats in the
							market today, and some may say they’re the standard at this point.
							Instead of the set it and forget it approach for a
							non-programmable thermostat, one’s able to change set points,
							schedules, and occupations settings from an app or website with a
							web-enabled thermostat.
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							How Much Savings?
						</h3>
						<p>
							With a Wi-Fi thermostat, you can save a lot of money without even
							knowing it compared to a non-Wi-Fi thermostat. With an older style
							thermostat, the HVAC runs based on the temperature and cooling
							setting enabled at the thermostat itself. If you’re away from the
							house, or you forget to change settings while the windows are
							open, the thermostat’s settings may keep the HVAC running. This
							adds to your electric bill. The amount of savings depends on one’s
							size of the house and lifestyle.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Which Type of Wi-Fi Thermostat?
						</h3>
						<p>
							The type of Wi-Fi thermostat one should choose is based on the
							type of equipment and one’s preference. Certain types of equipment
							(especially very high efficiency equipment) may require a
							proprietary thermostat so that the HVAC units work appropriately.
							If one has equipment with a conventional thermostat interface, one
							can choose almost any Wi-Fi thermostat available. The most popular
							are Nest and ecobee, although all manufacturers of HVAC equipment
							also have an equivalent.
						</p>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Potential Problems
						</h3>
						<p>
							Like any form of technology, it’s very important to read through
							the installation manual when setting up a Wi-Fi thermostat. If the
							thermostat isn’t communicating correctly, the HVAC equipment won’t
							function properly. For instance, Wi-Fi thermostats may require a
							COM wire which isn’t always present in older systems. The option
							is then to run an additional wire from the thermostat to the unit
							or to purchase an available accessory that compensates for the
							lack of a COM wire.
						</p>

						<NewsletterSignUp />
						{/* <CallToAction /> */}
					</div>
				</div>
			</div>
		</div>
	);
}
