import React from "react";
import Image from "next/image";
import Head from "next/head";
import NewsletterSignUp from "../../layout/newslettersignup";
import {
	PlusCircleIcon,
	MinusCircleIcon,
	ExclamationIcon,
} from "@heroicons/react/outline";

export default function HeatPump() {
	return (
		<div className="heatpump">
			<Head>
				<title>Heat Pumps | Tarclone</title>
				<meta name="robots" content="all" />
				<meta
					name="keywords"
					content="heat pumps, heat pump, boulder, air conditioner, advantages, homeowners, contractors, hvac, air conditioning, replace hvac, air filters, maintenance"
				/>
			</Head>

			<div className="relative py-4 md:py-16 bg-white overflow-hidden">
				<div className="hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full">
					<div
						className="relative h-full text-lg max-w-prose mx-auto"
						aria-hidden="true"
					>
						<svg
							className="absolute top-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="74b3fd99-0a6f-4271-bef2-e80eeafdf357"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
							/>
						</svg>
						<svg
							className="absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="f210dbf6-a58d-4871-961e-36d5016a0f49"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
							/>
						</svg>
						<svg
							className="absolute bottom-12 left-full transform translate-x-32"
							width={404}
							height={384}
							fill="none"
							viewBox="0 0 404 384"
						>
							<defs>
								<pattern
									id="d3eb07ae-5182-43e6-857d-35c643af9034"
									x={0}
									y={0}
									width={20}
									height={20}
									patternUnits="userSpaceOnUse"
								>
									<rect
										x={0}
										y={0}
										width={4}
										height={4}
										className="text-gray-200"
										fill="currentColor"
									/>
								</pattern>
							</defs>
							<rect
								width={404}
								height={384}
								fill="url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
							/>
						</svg>
					</div>
				</div>
				<div className="relative px-4 sm:px-6 lg:px-8">
					<div className="text-lg max-w-prose mx-auto">
						<h1>
							<span className="block text-base text-center text-blue-500 font-semibold tracking-wide uppercase">
								System Type & Info
							</span>
							<span className="my-2 mb-4 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
								Heat Pump System
							</span>
						</h1>
						<figure>
							<Image
								src="/images/resources/system/heatpump_resources.jpg"
								alt="Air Conditioners on side of House"
								width={1024}
								height={512}
							/>
						</figure>
					</div>
					<div className="mt-8 prose prose-indigo prose-lg text-gray-600 max-w-prose mx-auto">
						<h3 className="mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							What is a Heat Pump?
						</h3>
						<p>
							A heat pump utilizes electric cooling and electric heating. An
							outdoor heat pump contains a reversing valve that allows the
							refrigeration loop to reverse course depending on the need for
							heating or cooling. The outdoor heat pump connects to an indoor
							air handler which contains a fan and heat transfer coil.
						</p>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Advantages of a Heat Pump System
						</h3>
						<p>
							Deciding on a heat pump solution will carry the following
							advantages:
						</p>
						<ul className="mt-2 ml-5 space-y-2">
							<li className="flex items-center">
								<PlusCircleIcon className="w-6 h-6 flex-none text-green-500" />
								<p className="ml-4">
									An all-electric system for the sustainable future
								</p>
							</li>
							<li className="flex items-center">
								<PlusCircleIcon className="w-6 h-6 flex-none text-green-500" />
								<p className="ml-4">
									A simple replacement for an existing central A/C system
								</p>
							</li>
							<li className="flex items-center">
								<PlusCircleIcon className="w-6 h-6 flex-none text-green-500" />
								<p className="ml-4">
									Most systems work with Wi-Fi thermostats such as Nest and
									ecobee
								</p>
							</li>
						</ul>
						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Disadvantages of a Heat Pump System
						</h3>
						<p>Some negative characteristics of a heat pump system include:</p>
						<ul className="mt-2 ml-5 space-y-2">
							<li className="flex items-center">
								<MinusCircleIcon className="w-6 h-6 flex-none text-red-500" />
								<p className="ml-4">
									An additional electric heating coil will be needed in cold
									climates
								</p>
							</li>
							<li className="flex items-center">
								<MinusCircleIcon className="w-6 h-6 flex-none text-red-500" />
								<p className="ml-4">
									May be expensive to operate if local electric rates are high
								</p>
							</li>
						</ul>

						<h3 className="mt-4 mb-2 text-xl md:text-2xl font-semibold text-gray-800">
							Preventive Maintenance
						</h3>
						<p>
							Much like a car, you should do routine checks and maintenance to
							keep your HVAC system healthy:
						</p>
						<ul className="mt-2 ml-5 space-y-2">
							<li className="flex items-center">
								<ExclamationIcon className="w-6 h-6 flex-none text-yellow-500" />
								<p className="ml-4">
									Hire contractor to check unit before cooling season and
									heating season
								</p>
							</li>
							<ul className="mt-2 ml-10 list-decimal list-inside">
								<li>Ensure coils are clean</li>
								<li>Check refrigerant levels</li>
								<li>Diagnose any other problems</li>
							</ul>
							<li className="mt-2 flex items-center">
								<ExclamationIcon className="w-6 h-6 flex-none text-yellow-500" />
								<p className="ml-4">Change filter every three months</p>
							</li>
						</ul>
						<NewsletterSignUp />
						{/* <CallToAction /> */}
					</div>
				</div>
			</div>
		</div>
	);
}
