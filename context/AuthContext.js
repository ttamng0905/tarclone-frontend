import {createContext, useEffect, useRef, useState} from 'react';
import {API_URL, NEXT_URL} from "@/config/index";
import {useRouter} from "next/router";
import jsCookie from "js-cookie";

const AuthContext = createContext()

export const AuthProvider = ({children}) => {
    const [user, setUser] = useState(null)
    const [error, setError] = useState(null)
    const [token, setToken] = useState(null);
    const router = useRouter();
    const contractor = useRef();


    //Check if user is logged in
    const checkUserLoggedIn = async () => {

        contractor.current = JSON.parse(localStorage.getItem("user"));
        console.log(contractor);
        if (contractor.current) {
            setUser(contractor.current);
        }
        // } else {
        //     const res = await fetch(`${NEXT_URL}api/user`, {
        //         method: 'POST',
        //         headers: {
        //             Accept: "application/json",
        //             "Content-Type": "application/json"
        //         },
        //         body: JSON.stringify({
        //             token
        //         })
        //     })
        //     console.log(res);
        //     if(res){
        //         setUser(res.contractor);
        //         // await router.push('/')
        //     } else {
        //         setUser(null)
        //     }
        // }

    }
    useEffect(() => checkUserLoggedIn, []);
    //Register User
    const register = async ({company_name, email, password, password2, address, city, state, zip, phone, mobile, employees_number, founded, doEmergency}) => {
        const contractorRegistration = await fetch(`${API_URL}auth/contractorsignup`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                company_name, email, password, password2, address, city, state, zip, phone, mobile, employees_number, founded, doEmergency
            })
        })
            .then(response => {
                // console.log(response)
                return response.json();
            })
            .catch(err => console.log(err))
        // if(contractorRegistration){
        //     // res.status(200).json({user: contractorRegistration})
        // } else {
        //     // res.status(204).json({error: "Something went wrong"})
        // }
    }

    //Forgot Password
    // const forgot = async() => {
    //     const data = await fetch(`${API_URL}/auth/forgot-password`, {
    //         method: "PUT",
    //         headers: {
    //             Accept: "application/json",
    //             "Content-Type": "application/json",
    //         },
    //         body: JSON.stringify({
    //             email
    //         })
    //     })
    //         .then((response) => {
    //             console.log("forgot password response: ", response)
    //             return response.json();
    //
    //         })
    //         .then(data => {
    //             if (data.error) {
    //                 console.log(data.error)
    //             }
    //             else {
    //                 return data;
    //             }
    //         }).catch(err => {
    //             console.log(err)
    //         });
    //     if(data.message){
    //         res.status(200).json({message: forgot.message})
    //     } else {
    //         res.status(204).json({error: "Something wrong happened"})
    //     }
    // }

    //Login User
    const login = async (user) => {
        let contractorId;
        const email = user.email;
        const password = user.password;
        const contractorLogin = await fetch(`${API_URL}auth/contractorlogin`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email,
                password
            })
        })
            .then(response => {
                console.log("me");
                return response.json();
            })
            .then(response => {
                if (response['token']) {
                    contractorId = response['contractorId'];
                }
                return response
            })
            .catch(err => console.log(err))

        const userData = await fetch(`${API_URL}api/contractor/${contractorId}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
        })
            .then(response => {
                const data = response.json();
                return data;
            })
            .catch(err => console.log(err))

        console.table(userData["contractor"]);
        if (userData["contractor"]) {
            await setUser(userData["contractor"])
            await localStorage.setItem("user", JSON.stringify(userData["contractor"]));
        } else {
            setError(null)
        }
        if(userData["contractor"]){
             // @todo: Set Cookies
            setToken(contractorLogin.token);
            jsCookie.set('token', contractorLogin.token);
        }
    }
    //Logout User
    const logout = async () => {
        setUser(null);
        console.log("me");
        localStorage.removeItem("user");
    }

    return (
        <AuthContext.Provider value={{user, error, register, login, logout}}>
            {children}
        </AuthContext.Provider>

    )
}
export default AuthContext





