/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['images.unsplash.com', "www.gravatar.com"]
  },
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: "https://tarclone-backend.herokuapp.com/:path*",
      },
    ]
  },
}

module.exports = nextConfig
