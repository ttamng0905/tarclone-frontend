const dev = process.env.NODE_ENV !== 'production'

export const NEXT_URL = process.env.NEXT_PUBLIC_FRONTEND || "https://www.tarclone.com/"

export const API_URL = process.env.BACKEND || "https://tarclone-backend.herokuapp.com/"
